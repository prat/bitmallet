# BitMallet #

Bit-bashing code for SkyScreen.

Be warned: the code is  quite ugly at the moment.

### Dependencies ###

**PJ_RPI**

Required for performing GPIO operations.

Checkout from here: https://github.com/Pieter-Jan/PJ_RPI

Follow instructions in [README](https://github.com/Pieter-Jan/PJ_RPI/blob/master/README.md) to build and install.

**Important:** Comment out the ```#define RPI``` line and uncomment the line which reads ```#define RPI2``` before building.

**BitCatcher**

Receive frames over UDP from SkyScreen pattern generator.

Checkout from here: https://bitbucket.org/prat/bitcatcher.git

Follow instructions in [README](https://bitbucket.org/prat/bitcatcher/src/b81dbf719b492d385845ea5c678334093e0da57c/README.md?at=master) to build and install.

### Instructions ###

Create BitMallet folder, e.g. /home/<user>/BitMallet/

Do this:


```
> mkdir /home/<user>/BitMallet
> cd /home/<user>/BitMallet
> git clone https://prat@bitbucket.org/prat/bitmallet.git
> mkdir build
> cd build
> cmake ../src
> make
```

To run BitMallet, make sure you do it as sudo so it has permissions to write to the GPIO memory locations:
```
> sudo BitMallet
```