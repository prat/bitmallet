#define _GNU_SOURCE
#include "PixelTools.h"
#include <sys/time.h>
#include <sys/select.h>
#include <stdio.h>
#include <assert.h>
#include "PJ_RPI.h"
#include "raspi.h"

#define RANGE(IDX, UPTO) (int IDX = 0; IDX < UPTO; IDX++)

void update_pause() {
    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(STDIN_FILENO, &readfds);
    struct timeval timeout = {0, 0};
    int nready = select(STDIN_FILENO+1, &readfds, NULL, NULL, &timeout);
    assert(nready != -1 && "Select() failed");
    int clock_step = 10;
    if (nready) {
        char c = fgetc(stdin);
        switch (c) {
            case '=':
            case '+':
            case '\n':
                pixel_config.clock_pause += clock_step;
                printf("Clock pause is: %lu\n", pixel_config.clock_pause);
                break;
            case '-':
                if (pixel_config.clock_pause > clock_step){
                    pixel_config.clock_pause -= clock_step;
                    printf("Clock pause is: %lu\n", pixel_config.clock_pause);
                } else {
                    printf("Clock pause is: %lu, refusing to go lower\n", pixel_config.clock_pause);
                }
                break;
            default:
                break;
        }
    }
}


/** Do a basic skyscreen loop, from the raspberry pi image */
void skyscreenLoop() {                                                    // this is the main loop
    static uint8_t vane_arr[360][288][3];                                 // this will be our data
    static uint8_t *vane_arr_addr[360][288][3];                           // this will be our addresses of data
    for RANGE(row, height) for RANGE(col, width) {                        // we loop through all pixels,
        HEADER_PIXEL(header_data, vane_arr[row][col]);                    // pull our the RGB data
        for RANGE(color, 3) {
           vane_arr_addr[row][col][color] = &(vane_arr[row][col][color]); // and copy out the addresses
        }
    }

    int first_vane_angle = 0;                                             // we start from the first angle, 
                                                                          // of course
    long loops = 0;
    int megaloops = 0;
    while (megaloops < 10) {
        if (loops > 25*30){ 
            printf("Done 25 frames, and here's a random number: %d\n", rand());
	    megaloops++;
            loops = 0;
        }
        loops++;
        writeScreen(vane_arr, first_vane_angle);                     // write to the screen
        first_vane_angle = (first_vane_angle + 1) % 360;                  // and cycle around
        //update_pause();
    }
    printf("DONE!\n");
}

void setup(char use_rpi) {
    // this can be helpful to check C isn't playing silly buggers.
    // assert((long)(&(GPIO_SET)) == (long)((gpio.addr + 7)));
    // assert((long)(&(GPIO_SET)) == (long)((gpio.addr + 8)));
    

    for (int i = 0; i < 12; i++) pixel_config.data_pins[i] = default_data_pins[i];
    pixel_config.clock_pin = default_clock_pin;
    pixel_config.clock_pause = 1;

    if (use_rpi) {
        pixel_config.gpio_set = &(GPIO_SET);
        pixel_config.gpio_clr = &(GPIO_CLR);
	    assert(map_peripheral(&gpio) != -1 && "Could not map peripherals");
    } else {
        pixel_config.gpio_set = calloc(1, sizeof(uint32_t));
        pixel_config.gpio_clr = calloc(1, sizeof(uint32_t));
    }
}

void shutdown(char use_rpi) {
    if (!use_rpi) {
        free((void*)pixel_config.gpio_set);
        free((void*)pixel_config.gpio_clr);
    }
}
int main(int argv, char* argc[]){
    setup(0);
    skyscreenLoop();
    // unrechable
    shutdown(0);
}

