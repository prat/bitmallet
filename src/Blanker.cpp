#include "BitMallet.h"
#include "PJ_RPI.h"

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main() {
	byte headers[12] = {0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0};
	byte zeroes[12]  = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	CLOCK_PAUSE = 100;
	LINE_PAUSE = 100;

        if(map_peripheral(&gpio) == -1)
        {
            printf("Failed to map the physical GPIO registers into the virtual memory space.\n");

            return -1;
        }

	int p = 0;

        for (p = 0; p < 12; p++) {
                INP_GPIO(dataPins[p]);
                OUT_GPIO(dataPins[p]);

                INP_GPIO(clockPins[p]);
                OUT_GPIO(clockPins[p]);
        }

        clockBits = 0;

        for (p = 0; p < 12; p++) {
                clockBits = clockBits | (1 << clockPins[p]);
        }

	int i = 0;

	int d = 0;
	//for (d = 0; d < LINE_PAUSE; d++) ((void)0);

	writeLineHeader();
	writeLineHeader();
	for (i = 0; i < 288; i++) {
		writePixelBytes(headers);
		for (d = 0; d < 10; d++) ((void)0);
		writePixelBytes(zeroes);
		for (d = 0; d < 10; d++) ((void)0);
		writePixelBytes(zeroes);
		for (d = 0; d < 10; d++) ((void)0);
		writePixelBytes(zeroes);
		for (d = 0; d < 10; d++) ((void)0);
	}

	return 0;
}

void writePixelBytes(byte bytes[12]) {
	int i = 0;

	// buffer of bit values for each vane
	int bits;

	int vane;

	int mask = 0x80; // start at the most significant bit

	// loop through each bit position in the bytes
	for (i = 0; i < 8; i++) {
		bits = 0;
		
		// for all 12 vanes, collect the bit value at the given position into a single integer
		for (vane = 0; vane < 12; vane++) {
			// get value of the current bit for the current vane
			int bit = (bytes[vane] & mask) ? 1 : 0;

			// (bit << vane): shift this value to the left by the current vane number and add it to the bit buffer
			bits = bits | (bit << vane);
		}

		// write out the bits for all vanes in one go
		writeDataBits(bits);

		mask = mask >> 1; // go to the next significant bit
	}
}

void writeDataBits(int bits) {
	int mask, pin;
	// buffers for setting which pins should be set to high, and which should be set to low
	int high = 0, low = 0;

	// set clock pins to low
	low = clockBits;
	//pin number
	pin = 0;
	// start at least significant bit, and then work towards the first
	mask = 1;
	for (pin = 0; pin < 12; pin++) {
		// check the value of the bit at the current pin's mask position
		if (bits & mask) {
			//data bits set to 1 should be added to the high pins buffer
			high = high | (1 << dataPins[pin]);
		} else {
			//data bits set to 0 should be added to the low pins buffer
			low = low | (1 << dataPins[pin]);
		}

		// shift the mask bit to the left by 1 position to process the next bit
		mask = (mask << 1);
	}

	// set all pins with 0 value bits to low (including the clock)
	GPIO_CLR = low;

	// set all pins with 1 value bits to excluding (excluding the clock)
	GPIO_SET = high;

	// pusle the clock
	GPIO_SET = clockBits;

	// do a little dance and then you drink a little water
	int delay = 0;
	for (delay = 0; delay < CLOCK_PAUSE; delay++) ((void)0);
}

// write 32 zeroes in a row to indicate the sart of a new line
void writeLineHeader() {
	int p = 0;
	for (p = 0 ; p < FRAME_HEADER_BITS; p++) writeDataBits(0);
}

// write 32 ones in a row to indicate the end of a line
// not sure if this s actually needed
// it's currently not called from anywhere, but I'm leaving the code here just in case
void writeLineFooter() {
	int p = 0;
	for (p = 0 ; p < FRAME_FOOTER_BITS; p++) writeDataBits(1);
}

// write out the header for a given pizel position
// this is to allow for differing brightness levels along the length of a vane
void writePixelHeader(int i) {
	byte bytes[12];

	int vane = 0;

	for (vane = 0; vane < 12; vane++) {
		bytes[vane] = pixelHeaders[i];
	}

	writePixelBytes(bytes);
}
