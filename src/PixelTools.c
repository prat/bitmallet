/**
 * There are two important macro-variables:
 * - TEST: if set, we'll run tests 
 * - HEAVYTEST: if set, along with tests, we'll run 
 *   more intensive tests, that can throw out profiling efforts
 */ 
#include <stdint.h>
#include "PixelTools.h"

uint32_t default_data_pins[12] = {
    1 << 17,
    1 << 27,
    1 << 22,
    1 << 23,
    1 << 24,
    1 << 25,
    1 << 8,
    1 << 7,
    1 << 5,
    1 << 6,
    1 << 12,
    1 << 13
};
uint32_t default_clock_pin = (1 << 0);
int default_clock_pause = 0;

#define RANGE(IDX, UPTO) (int IDX = 0; IDX < UPTO; IDX++)
/**
 * Set the GPIO pins based on a set of values 
 */
void setGpio(uint32_t values) {
    *(pixel_config.gpio_clr) = (!values);
    *(pixel_config.gpio_set) =   values ;
}

void fst_writeByte(uint8_t vane_values[12]) {                          // We'll write these bytes out to each of the 12
    uint8_t vane_cpy[12];                                              // vanes. So first we copy the vanes, because we
    for RANGE(i, 12) vane_cpy[i] = vane_values[i];                     // will be doing destructive overwrites of them

    for RANGE(mask_step, 8) {                                          // next, we go through bit 1, 2 etc for each vane
        uint32_t values = 0;                                           // and build up the int we'll send to the pins
	for RANGE(vane, 12) {                                          // then, for each vane,
            if (vane_cpy[vane] & 1)                                    // we inspect the LSB of it's value
                values |= pixel_config.data_pins[vane];                // and if it is one, we set the corresponding 
	                                                               // bit in the values integer
	    vane_cpy[vane] = vane_cpy[vane] >> 1;                      // then we shift it left by one (this mean we 
	}                                                              // avoid expensive mask shifting)

        setGpio(values);                                               // then we set the GPIO pins
        setGpio(values | pixel_config.clock_pin);                      // and we set them again, with the clock high
        for RANGE(delay, pixel_config.clock_pause) {                   // then, for the appropriate number of cycles
            __asm__("NOP");                                            // execute NOOP
	}
    }
}

// Write the first bit, everywhere.
// useful for writing 0xfffffff or 
// 0x00000000
void writeLSBValue(uint8_t value){
    uint32_t values = 0;                                               // This will be the 32b uint we send to the 
                                                                       // GPIO pins
    for RANGE(vane, 12) {                                              // and for each vane, we go through and set the
        if (value & 1)                                                 // value for the vane int, to be the LSB of the
            values = values | pixel_config.data_pins[vane];            // value we were passed in.
    }

    for RANGE(i, 8) {                                                  // and because we need to do this 8 times for 
        setGpio(values);                                               // the 8 bits in a byte, we have an 8 step loop.
        setGpio(values | pixel_config.clock_pin);
        for RANGE(delay, pixel_config.clock_pause) {                   // followed by our pause.
            __asm__("NOP");
        }
    }
}


void writeColors(uint8_t fields[3][12]) {
    writeLSBValue(0xF);
    for RANGE(color, 3) fst_writeByte(fields[color]);
}


/** Write out 32 zeros */
void writeLineStartFrame() {
    for RANGE(i, 4) writeLSBValue(0xF);
}

/** Write out 32 ones */
void writeLineEndFrame() {
    for RANGE(i, 4) writeLSBValue(0x0);
}

/**
 * writeFullVanes writes 12 full vanes, from vane data, 
 * including the top and the tail.
 *
 * It's pretty straight forward :)
 */
void writeFullVanes(uint8_t vane_data[288][3][12]) {
    writeLineStartFrame();
    for RANGE(mag, 288) writeColors(vane_data[mag]);
    writeLineEndFrame();
}

/** writeScreen will do two things:
 * 1. it'll work out which vanefulls to blast out
 * 2. and the we'll blast them out.
 *
 * This is everything that's needed in order to write a full screen-full,
 * thus the name.
 */
void writeScreen(uint8_t vane_data[360][288][3], int first_vane_angle) {
    static uint8_t vane_arr[288][3][12];
    for RANGE(mag, 288) {
        for RANGE(vane, 12) {
            int actual_vane = (first_vane_angle + vane * 30) % 360;
            vane_arr[mag][0][vane] = vane_data[actual_vane][mag][0];
            vane_arr[mag][1][vane] = vane_data[actual_vane][mag][1];
            vane_arr[mag][2][vane] = vane_data[actual_vane][mag][2];
        }
    }
    writeFullVanes(vane_arr);
}

#ifdef TEST
#include <stdio.h>
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"
int main(int argv, char* argc[]) {
    for RANGE(i, 10) 
    	printf("%sNO TESTS EXIST%s\n", KRED, RESET);
}
#endif
