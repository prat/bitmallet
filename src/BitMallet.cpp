#include "BitMallet.h"
#include "PJ_RPI.h"

//#include "raspi.h"

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void sig_handler(int signo) {
	if (signo == SIGINT) {
    	printf("\n\nStopping\n");

    	byte headers[12] = {0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0};
    	byte zeroes[12]  = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

		int i = 0;

		int d = 0;
		for (d = 0; d < LINE_PAUSE; d++) ((void)0);

		writeLineHeader();
		writeLineHeader();
		for (i = 0; i < PIXELS_PER_STRAND; i++) {
			writePixelBytes(headers);
			for (d = 0; d < 10; d++) ((void)0);
			writePixelBytes(zeroes);
			for (d = 0; d < 10; d++) ((void)0);
			writePixelBytes(zeroes);
			for (d = 0; d < 10; d++) ((void)0);
			writePixelBytes(zeroes);
			for (d = 0; d < 10; d++) ((void)0);
		}

		last_frame(serv);
		void *result = NULL;
	    pthread_join(server_thread, &result);
		free_server(serv);

		exit(0);
	} else if (signo == SIGUSR1) {
		readBrightnessConfig();
	}
}

int debug = 0;


int main()
{

    time_t timer;
    char buffer[26];
    struct tm* tm_info;

    time(&timer);
    tm_info = localtime(&timer);

    strftime(buffer, 26, "%Y:%m:%d %H:%M:%S", tm_info);

	if (signal(SIGINT, sig_handler) == SIG_ERR) printf("\ncan't catch SIGINT\n");
	if (signal(SIGUSR1, sig_handler) == SIG_ERR) printf("\ncan't catch SIGUSR1\n");

	debug = 0;

	pixelHeaders = (byte*)calloc(PIXELS_PER_STRAND, sizeof(byte));

	readBrightnessConfig();

	if(map_peripheral(&gpio) == -1) 
	{
    	printf("Failed to map the physical GPIO registers into the virtual memory space.\n");

      	return -1;
    }

	int p = 0;

	for (p = 0; p < 12; p++) {
		INP_GPIO(dataPins[p]);
		OUT_GPIO(dataPins[p]);

		INP_GPIO(clockPins[p]);
                OUT_GPIO(clockPins[p]);
	}

	int x = 0;

	// work out how long to pause between each angle position for a vane
	/*
		Start with 1,000,000,000 nanoseconds in 1 second
		Divide by 1 for the number of revolutions per second
		Divide by 10 for the number of frames per revolution
		Divide by 30 for the number of angular positions per frame
	*/
	POSITION_PAUSE = (long)(1000000000.0f / 1.0f / 10.0f / 30.0f);

	printf("Nanoseconds between vane positions: %ld\n", POSITION_PAUSE);

	// Construct  an INT to address all of the clock pins in one go
	clockBits = 0;

	for (p = 0; p < 12; p++) {
		clockBits = clockBits | (1 << clockPins[p]);
	}

	displayImage();

	return 0;
}

void* runServer(void *arg) {
    initalize_server(serv, "5555");
    while(!exit_time) receive_frames(serv);
    terminate_server(serv);
    return NULL;
}

void displayImage() {
	int h, w;

	byte bytes[12];

	// GIMP outputs bitmaps as C-arrays in a wierd way,
	// so the code below loops through it using the HEADER_PIXEL macro it provides
	// and puts the image data into a nicer to work with 360x288x3 byte array

	int width = 288;

	int rgb[3];
	/*for (h = 0; h < height; h++) {
		for (w = 0; w < width; w++) {
			// NOTE: HEADER_PIXEL is a GIMP generated macro, it is unrelated writing pixel header bits for the vanes
			HEADER_PIXEL(header_data, rgb);

			frame[h][w][0] = rgb[0];
			frame[h][w][1] = rgb[1];
			frame[h][w][2] = rgb[2];
		}
	}*/

	int angle = 0;

	int colour = 0;

	int vane = 0;

	int pixel = 0;

	int headers[12];

	serv = new_server(0);

	if (pthread_create(&server_thread, NULL, &runServer, NULL)) {
        assert(false && "Thread failed to start");
    }

    first_frame(serv);

	int f = 0;

	while (1) {
		// Tell the receiver thread to get the next frame (if the data is there)
		printf("Entering frame stuff\n");
		frame_pause(serv);

		printf("Frame: %d\n", f);

		// 30 different positions for each vane for a frame
		for (angle = 0; angle < 30; angle++) {
			recordTimestamp();

			writeLineHeader();

			// go through each of the 288 pixels in a line
			for (pixel = width-1; pixel >= 0; pixel--) { //
				writePixelHeader(pixel);

				for (colour = 2; colour >= 0; colour--) {
					// compile an array of byte values for the current andgle and pixel position on all 12 vanes
					for (vane = 0; vane < 12; vane++) {
						//bytes[vane] = serv->output_frame[(((vane * 30) + angle) * 360) + (pixel * 288) + colour];
						int target_px = ((vane * 30)+angle)*288*3 + pixel*3 + colour;
						bytes[vane] = serv->output_frame[target_px];


						//frame[angle + (vane * 30)][pixel][colour];
					}

					writePixelBytes(bytes);
				}
			}

			//waitForVaneRotation();
		}

		// rotate the data and clock pin assignments for vanes before starting the next frame
		//rotateVanePositions();	

		f++;
	}
}

void  recordTimestamp() {
	// find out the timestamp at the start of the delay loop
	// use the monotic clock because we care about the elapsed time
	clock_gettime(CLOCK_MONOTONIC_RAW, &start_time);
}

// loop around and do nothing until the vanes have moved into their next positions
void waitForVaneRotation() {
	int elapsed_nanoseconds = 0;

	// loop around until at least POSITION_PAUSE microseconds have elapsed
	while (elapsed_nanoseconds < POSITION_PAUSE) {
		clock_gettime(CLOCK_MONOTONIC_RAW, &current_time);

		elapsed_nanoseconds = current_time.tv_nsec - start_time.tv_nsec;
	}
}

// At the end of each frame, rotate the data pins array
// eg: what was vane 11 is now vane 0, vane 0 is now vane 1, vane 1 is now vane 2, etc...
// this makes sure that the image remains static and doesn't appear to rotate along with the vanes
void rotateVanePositions() {
	int p, tmpData, tmpClock;

	tmpData = dataPins[0];
        tmpClock = clockPins[0];

	for (p = 0; p < 11; p++) {
		dataPins[p] = dataPins[p + 1];
		clockPins[p] = clockPins[p + 1];
	}

	dataPins[11] = tmpData;
	clockPins[11] = tmpClock;
}

void writePixelBytes(byte bytes[12]) {
	int i = 0;

	// buffer of bit values for each vane
	int bits;

	int vane;

	int mask = 0x80; // start at the most significant bit

	// loop through each bit position in the bytes
	for (i = 0; i < 8; i++) {
		bits = 0;

		// for all 12 vanes, collect the bit value at the given position into a single integer
		for (vane = 0; vane < 12; vane++) {
			// get value of the current bit for the current vane
			int bit = (bytes[vane] & mask) ? 1 : 0;

			// (bit << vane): shift this value to the left by the current vane number and add it to the bit buffer
			bits = bits | (bit << vane);
		}

		// write out the bits for all vanes in one go
		writeDataBits(bits);

		mask = mask >> 1; // go to the next significant bit
	}

	if (debug == 1) {
		printf("\n");
		exit(0);
	}
}

void writeDataBits(int bits) {
	int mask, pin;
	// buffers for setting which pins should be set to high, and which should be set to low
	int high = 0, low = 0;

	// set clock pins to low
	low = clockBits;
	//pin number
	pin = 0;
	// start at least significant bit, and then work towards the first
	mask = 1;
	for (pin = 0; pin < 12; pin++) {
		// check the value of the bit at the current pin's mask position
		if (bits & mask) {
			//data bits set to 1 should be added to the high pins buffer
			high = high | (1 << dataPins[pin]);
		} else {
			//data bits set to 0 should be added to the low pins buffer
			low = low | (1 << dataPins[pin]);
		}

		// shift the mask bit to the left by 1 position to process the next bit
		mask = (mask << 1);
	}

	/*if (debug == 1) {
		char *binary;

		binary = itoa(high, 2);
		printf("%s", binary);

		binary = itoa(low, 2);

		printf(" - %s\n", binary);
	}*/

	// set all pins with 0 value bits to low (including the clock)
	GPIO_SET = low;

	// set all pins with 1 value bits to excluding (excluding the clock)
	GPIO_CLR = high;

	// pusle the clock
	GPIO_CLR = clockBits;

	// do a little dance and then you drink a little water
	int delay = 0;
	for (delay = 0; delay < CLOCK_PAUSE; delay++) ((void)0);
}

char* itoa(int val, int base){
	
	static char buf[32] = {0};
	
	int i = 30;
	
	for(; val && i ; --i, val /= base)
	
		buf[i] = "0123456789abcdef"[val % base];
	
	return &buf[i+1];
	
}

// write 32 zeroes in a row to indicate the sart of a new line
void writeLineHeader() {
	int p = 0;
	for (p = 0 ; p < FRAME_HEADER_BITS; p++) writeDataBits(0);
	if (debug == 1) printf("\n");
}

// write 32 ones in a row to indicate the end of a line
// not sure if this s actually needed
// it's currently not called from anywhere, but I'm leaving the code here just in case
void writeLineFooter() {
	int p = 0;
	for (p = 0 ; p < FRAME_FOOTER_BITS; p++) writeDataBits(1);
	if (debug == 1) printf("\n");
}

// write out the header for a given pizel position
// this is to allow for differing brightness levels along the length of a vane
void writePixelHeader(int i) {
	byte bytes[12];

	int vane = 0;

	for (vane = 0; vane < 12; vane++) {
		bytes[vane] = pixelHeaders[i];
	}

	writePixelBytes(bytes);

	if (debug == 1) printf("\n");
}

void debugByte(byte p) {
	int i = 0;

	byte b = p;

	for (i = 0; i < 8; i++) {
		//writeDataBit(b & 0x80);

		if ((b & 0x80) == 0x80) {
			printf("1");
		} else {
			printf("0");
		}

		b = b << 1;
	}

	/*byte mask = 0x80;

	for (mask = 0x80; mask > 0; mask = (mask >> 1)) {
		//writeDataBit(mask & p);
		if ((mask & b) == 1) {
			if (debug == 1) printf("1");
		} else {
			if (debug == 1) printf("0");
		}
	}*/

	//printf("\n");
}




void readLine(int count, FILE* f, char* l) {
	while(1) {
		memset(l, 0, sizeof(char) * 11);

		if (fgets(l, count, f) == NULL) exit(EXIT_FAILURE);

		if ((l[0] != '\n') && (l[0] != '\r') && (l[0] != '\0')) break;
	}
}

void readBrightnessConfig() {
	FILE*  config;
	char*  line;
	size_t size = 11;
	int baseline;

	config = fopen("/home/pi/BitMallet/conf/bitmallet.config", "r");
	if (config == NULL) {
		printf("Could not open config file\n");

		exit(EXIT_FAILURE);
	}

	//memset(line, 0, sizeof(char) * 11);

	line = (char*)calloc(sizeof(char), 11);

	if (fgets(line, 5, config) == NULL) exit(EXIT_FAILURE);

	CLOCK_PAUSE = atoi(line);

	printf("CLOCK PAUSE: %d\n", CLOCK_PAUSE);

	while(1) {
		memset(line, 0, sizeof(char) * 11);

		if (fgets(line, 5, config) == NULL) exit(EXIT_FAILURE);

		if ((line[0] != '\n') && (line[0] != '\r') && (line[0] != '\0')) break;
	}

	//if (fgets(line, 1, config) == NULL) exit(EXIT_FAILURE);	

	LINE_PAUSE = atoi(line);

	printf("FRAME PAUSE: %d\n", LINE_PAUSE);

	//if (fgets(line, 1, config) == NULL) exit(EXIT_FAILURE);

	while(1) {
		memset(line, 0, sizeof(char) * 11);

		if (fgets(line, 3, config) == NULL) exit(EXIT_FAILURE);

		if ((line[0] != '\n') && (line[0] != '\r') && (line[0] != '\0')) break;
	}

	//if (fgets(line, 3, config) == NULL) exit(EXIT_FAILURE);

	baseline = atoi(line);

	printf("Default brightness: %d\n", baseline);

	pixelHeaders = (byte*)calloc(PIXELS_PER_STRAND, sizeof(byte));
	
	//memset(pixelHeaders, (byte)(0xE3), PIXELS_PER_STRAND); // * sizeof(byte)

	//int i;
	//for (i = 0; i < PIXELS_PER_STRAND; i++) {
		//pixelHeaders[i] = (byte)(0xE3);
	//}

	memset(pixelHeaders, (unsigned char)(0xE0 | (baseline & 0x1F)), PIXELS_PER_STRAND * sizeof(byte));

	int l = 0;

	while (1) {
		memset(line, 0, sizeof(char) * 11);

		if (fgets(line, 11, config) == NULL) break;
		//int read = getline(&line, &size, config) == -1;

		/*(printf("Read %d chars from file\n", read);

		if (read < 0) break;

		if (read == 0) continue;*/

		if ((line[0] == '\n') || (line[0] == '\r') || (line[0] == '\0')) continue;

		int start = atoi(line);
		int end = atoi(line + 4);
		int value = atoi(line + 8);

		printf("Brightness range start: %d end: %d value: %d\n", start, end, value);

		int i;

		if ((start >= 0) && (end < PIXELS_PER_STRAND) && (value >= 0) && (value < 32)) {
			for (i = start; i <= end; i++) {
				pixelHeaders[i] = (byte)(0xE0 | (value & 0x1F));
			}
		}

		l++;
	}

	fclose(config);

	free(line);

	int i = 0;
	int mask = 0x80;

	/*printf("\n\n");

	for (i = 0; i < PIXELS_PER_STRAND; i++) {
		printf("%d) ", i);

		for (mask = 0x80; mask > 0; mask = (mask >> 1)) {
			if ((mask & pixelHeaders[i]) == 0) {
				printf("0");
			} else {
				printf("1");
			}
		}

		printf("\n");

		//mask = 0x80;
	}

	printf("\n\n");*/

	//exit(EXIT_SUCCESS);
}


/*
old test code

void displayImage2() {
	int h, w;

	byte bytes[12];

	int rgb[3];
	for (h = 0; h < height; h++) {
		for (w = 0; w < width; w++) {
			// NOTE: HEADER_PIXEL is a GIMP generated macro, it is unrelated writing pixel header bits for the vanes
			HEADER_PIXEL(header_data, rgb);

			frame[h][w][0] = rgb[0];
			frame[h][w][1] = rgb[1];
			frame[h][w][2] = rgb[2];
		}
	}

	int angle = 0;

	int colour = 0;

	int vane = 0;

	int pixel;

	//while (1) {
		int line = 0;

		for (line = 0; line < 360; line++) {
			writeLineHeader();

			for (pixel = 0; pixel < 288; pixel++) {
				writePixelHeader(pixel);

				for (colour = 2; colour >= 0; colour--) {
					for (vane = 0; vane < 12; vane++) {
						bytes[vane] = 0; // frame[line][pixel][colour];
					}

					bytes[3] = frame[line][pixel][colour];

					debug = 0;

					writePixelBytes(bytes);
				}

				//printf("\n");
			}

			//printf("\n");

			
		}

	sig_handler(SIGINT);
	//}
}

*/
