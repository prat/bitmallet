#include "BitCatcher.h"

#include <time.h>
#include <stdio.h>
#include <pthread.h>

#ifndef _BITMALLET_H_

#define _BITMALLET_H_

#define BYTES_PER_PIXEL 3
#define PIXELS_PER_STRAND 288

#define FRAME_HEADER_BITS 32
#define FRAME_FOOTER_BITS 32
#define PIXEL_HEADER_BITS 8

int CLOCK_PAUSE;
int LINE_PAUSE;

long POSITION_PAUSE;

// ribbon cable wired to the left
int dataPins[12] = {4, 15, 18, 22, 24, 9, 11, 7, 6, 13, 16, 20};
int clockPins[12] = {14, 17, 27, 23, 10, 25, 8, 5, 12, 19, 26, 21};

// ribbon cable wired to the right
//int dataPins[12] = {14, 15, 17, 23, 24, 25, 8, 7, 12, 13, 19, 26};
//int clockPins[12] = {4, 18, 27, 22, 10, 9, 11, 5, 6, 16, 20, 21};

int clockBits;

struct timespec start_time, current_time;

typedef unsigned char byte;

byte frame[360][288][3];

byte* pixelHeaders;

Server serv;

pthread_t server_thread;

bool exit_time = 0;


void readBrightnessConfig();
void *runServer(void *arg);
void displayImage();
void rotateVanePositions();
void recordTimestamp();
void waitForVaneRotation();
void writeLineHeader();
void writeLineFooter();
void writePixelHeader(int i);
void writePixelBytes(byte *bytes);
void writeDataBits(int bits);
void debugByte(byte p);

char* itoa(int val, int base);


void sig_handler(int signo);

// log file handle
FILE* lgf;

void startLogging();

#endif
