#include <stdint.h>
#pragma once
#ifndef PIXEL_TOOLS
#define PIXEL_TOOLS
/**
 * data_pins are one at the bit of each of the pi's GPIO pints
 */
typedef struct PixelConfig {
    volatile uint32_t *gpio_clr;
    volatile uint32_t *gpio_set;
    uint32_t data_pins[12];
    uint32_t clock_pin;
    unsigned long clock_pause;
} PixelConfig;

extern uint32_t default_data_pins[12];
extern uint32_t default_clock_pin;
extern int default_clock_pause;


/**
 * All config are read from this struct. You MUST update it.
 */
PixelConfig pixel_config;


/** writeScreen will do two things:
 * 1. it'll work out which vanefulls to blast out
 * 2. and the we'll blast them out.
 *
 * This is everything that's needed in order to write a full screen-full,
 * thus the name.
 */
void writeScreen(uint8_t vane_data[360][288][3], int first_vane_angle);
#endif
